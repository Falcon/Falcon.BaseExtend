﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Falcon.StringExtend.Tests
{
    [TestClass()]
    public class StringExtendTests
    {
        [TestMethod()]
        public void IsNullOrEmptyTest() {
            var str = "123";
            Assert.IsFalse(str.IsNullOrEmpty());
            Assert.IsTrue(str.IsNotNullOrEmpty());

            str = "";
            Assert.IsTrue(str.IsNullOrEmpty());
            Assert.IsFalse(str.IsNotNullOrEmpty());

            str = null;
            Assert.IsTrue(str.IsNullOrEmpty());
            Assert.IsFalse(str.IsNotNullOrEmpty());
        }
    }
}