var ggwsClient = {
    //公共卫生服务地址
    baseUrl: "http://localhost:9100/",

    //本地存储
    storage: function (k, v) {
        var sfun = window.sessionStorage ?
            function (k, v) { window.sessionStorage.setItem(k, v); } :
            function (k, v) { $.cookie(k, v); };
        var lfun = window.localStorage ?
            function (k) { return window.sessionStorage.getItem(k); } :
            function (k) { return $.cookie(k); };
        return v == undefined ? JSON.parse(lfun(k)) : sfun(k, JSON.stringify(v));
    },
    //发送请求获取api数据
    runapi: function (url, data, callback) {
        url = url.linkBaseUrl();
        data = typeof data == 'object' ? JSON.stringify(data) : data;
        let opt = {
            method: "post",
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: data,
            credentials: 'omit',
            mode: 'cors',
        };
        let cs = function (r) {
            if (r.status >= 200 && r.status < 300) {
                return r;
            }
            const error = new Error(r.statusText);
            error.response = r;
            throw error;
        }
        return fetch(url, opt)
            .then(cs)
            .then(resp => resp.json())
            .then(j => {
                if (callback) callback(j);
                return j;
            })
            .catch(e => console.error(e));
    },
    //获取html页面
    page: function () {
        var data = [];
        return {
            load: function (eid, url, state, s, c, e) {
                var op = {
                    data: state,
                };
                op.success = s || function () { };
                op.cancel = c || function () {
                    console.log("默认cancel执行");
                    $(eid).html("");
                };
                op.error = e || function (e) {
                    throw e;
                };
                data.push(op);
                fetch(url).then(r => r.text()).then(h => $(eid).html(h))
                    .then(() => history.pushState(state, "", url));
            },
            getfather: function () {
                return data.pop();
            },
        };
    }(),
    //将flag整数转换为数组
    FalgToArray: function (f) {
        var r = [];
        f = Number(f);
        if (typeof f != "number" || f == 0) {
            return r;
        }
        var c = 1;
        do {
            if ((f & c) == c) {
                r.push(c);
            }
            c = c * 2;
        } while (f >= c);
        return r;
    },
    //将数组转换为flag整数
    ArrayToFlag: function (arr) {
        var f = 0;
        for (var i = 0; i < arr.length; i++) {
            var v = Number(arr[i]);
            if (typeof v == "number") {
                f = f + v;
            }
        }
        return f;
    },
}

//为url字符串链接基础地址
String.prototype.linkBaseUrl = function () {
    var url = this;
    if (url.startsWith("http://") || url.startsWith("https://")) {
        return url;
    }
    if (url.startsWith("/")) {
        url = url.substr(1);
    }
    var baseUrl = ggwsClient.baseUrl;
    if (baseUrl.endsWith("/")) {
        baseUrl = baseUrl.substr(0, baseUrl.length - 1);
    }
    return baseUrl + "/" + url;
};

//设置cookie按照原样存储，不对值进行编码
$.cookie.raw = true;

//日期时间格式1 yyyy年mm月dd日
Vue.filter('dtf1', function (value) {
    var d = new Date(value);
    if (isNaN(d.getTime())) { return "时间格式错误！"; }
    return d.getFullYear() + "年" + (d.getMonth() + 1) + "月" + d.getDate() + "日";
});
//日期时间格式2 yyyy-mm-dd
Vue.filter('dtf2', function (value) {
    var d = new Date(value);
    if (isNaN(d.getTime())) { return "时间格式错误！"; }
    return d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
});
//计算记录状态Ggws.Database.EnumState枚举，生成状态字符串
Vue.filter('DataState', function (value) {
    if (typeof value != "number") {
        return "状态错误！";
    }
    var r = "";
    r = r + ((value & 1) == 1 ? "有效," : "无效,");
    r = r + ((value & 2) == 2 ? "已删除," : "");
    return r.substring(0, r.length - 1);
});
//转换男女标志
Vue.filter('Gender', function (value) {
    return value == 1 ? "男" : value == 2 ? "女" : "";
})