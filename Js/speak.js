var speak={};
speak.speak=function(text='html 文字输出语音'){
	if(window.speechSynthesis){
		const synth=window.speechSynthesis;
		const msg=new SpeechSynthesisUtterance();
		msg.text=text;	//播放内容
		msg.lang='zh-CN';	//使用中文
		msg.volume=1;		//音量
		msg.rate=1;			//语速
		msg.pitch=1.5;		//音高
		//msg.voice=this.getWindowVoice();	//使用本地语音
		synth.speak(msg);	//播放
	}
};