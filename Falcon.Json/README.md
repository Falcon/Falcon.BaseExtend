# Falcon.Cache
Json常用方法扩展。    

**Nuget URL:**    
<https://www.nuget.org/packages/Falcon.StringExtend/>  

将Json字符串转换为T类型对象：  
`String.ToObject<T>()`   

将一个对象序列化为Json字符串：  
`String.IsIsNotNullOrEmptyNullOrEmpty()`   
