﻿using System.Text.Json;

namespace Falcon.Json
{
    /// <summary>
    /// Json扩展
    /// </summary>
    public static class JsonExtend
    {
        /// <summary>
        /// 将Json字符串转换为对象
        /// </summary>
        /// <typeparam name="T">对象的类型</typeparam>
        /// <param name="str">Json字符串</param>
        /// <returns>对象</returns>
        public static T ToObject<T>(this string str) => JsonSerializer.Deserialize<T>(str);

        /// <summary>
        /// 把一个对象序列化为字符串
        /// </summary>
        /// <typeparam name="T">对象的类型</typeparam>
        /// <param name="obj">要系列化的对象</param>
        /// <returns>Json字符串</returns>
        public static string ToJsonString<T>(this T obj) => JsonSerializer.Serialize(obj);

    }
}
