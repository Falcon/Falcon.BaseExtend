# Falcon.Cache
字符串常用方法扩展。    

**Nuget URL:**    
<https://www.nuget.org/packages/Falcon.StringExtend/>  

返回字符串是否为空：  
`String.IsNullOrEmpty()`   

返回字符串是否不为空：  
`String.IsIsNotNullOrEmptyNullOrEmpty()`   
