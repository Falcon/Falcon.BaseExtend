﻿namespace Falcon.StringExtend
{
    /// <summary>
    /// 字符串对象扩展
    /// </summary>
    public static class StringExtend
    {
        /// <summary>
        /// 字符串是否为空。包括null,empty和空字符串。
        /// </summary>
        /// <param name="str">字符串</param>
        /// <returns>如果为Null,empty或空字符串返回true，否则false</returns>
        public static bool IsNullOrEmpty(this string str) => string.IsNullOrEmpty(str) || str == "";

        /// <summary>
        /// 字符串是否非空。跟IsNullOrEmpty方法相反。
        /// </summary>
        /// <param name="str">字符串</param>
        /// <returns>跟IsNullOrEmpty方法相反。</returns>
        public static bool IsNotNullOrEmpty(this string str) => !IsNullOrEmpty(str);
    }
}
