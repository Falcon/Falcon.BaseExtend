﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Falcon.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Falcon.Json.Tests
{
    [TestClass()]
    public class StringExtendTests
    {
        [TestMethod()]
        public void ToObjectTest() {
            var obj = JsonTestClass.GetObj();
            var str = obj.ToJsonString();
            var obj1 = str.ToObject<JsonTestClass>();
            Assert.AreEqual(obj,obj1);
        }

        class JsonTestClass
        {
            public int Id { get; set; }
            public string Name { get; set; }

            public static JsonTestClass GetObj() {
                return new JsonTestClass {
                    Id = 1,
                    Name = "abc",
                };
            }
            public override bool Equals(object obj) {
                if(obj is JsonTestClass o) {
                    return this.Id == o.Id && this.Name == o.Name;
                }
                return false;
            }

            public override int GetHashCode() {
                throw new NotImplementedException();
            }
        }
    }
}